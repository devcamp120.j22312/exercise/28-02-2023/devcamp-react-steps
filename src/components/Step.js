import { Component } from "react";

class Step extends Component {

    render() {
        const steps = [
            { id: 1, title: 'Hello World', content: 'Welcome to learning React!' },
            { id: 2, title: 'Installation', content: 'You can install React from npm.' },
            { id: 3, title: 'Create react app', content: 'Run create-react-app to run project.' },
            { id: 4, title: 'Run init project', content: 'Cd into project and npm start to run project.' },
        ];

        return (
            <>
                {steps.map((value, index) => {
                    return <ul key={index}> <b>Render Props</b>
                        <li><b>Id:</b> {value.id}</li>
                        <li><b>Title:</b> {value.title}</li>
                        <li><b>Content:</b> {value.content}</li>
                    </ul>
                })}
            </>
        )
    }
}
export default Step;