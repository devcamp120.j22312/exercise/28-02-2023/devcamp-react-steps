import logo from './logo.svg';
import './App.css';
import Step from './components/Step';

function App() {
  return (
    <div>
      <Step />
    </div>
  );
}

export default App;
